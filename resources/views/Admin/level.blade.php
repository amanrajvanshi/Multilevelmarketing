<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title>Golden Life Foundation </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords"
        content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
    <meta name="author" content="Codedthemes" />
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('admin_assets/img/favicon.png')}}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{asset('admin_assets/pages/waves/css/waves.min.css')}}" type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/bootstrap/css/bootstrap.min.css')}}">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{asset('admin_assets/pages/waves/css/waves.min.css')}}" type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/icon/themify-icons/themify-icons.css')}}">
    <!-- font-awesome-n -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/font-awesome-n.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/font-awesome.min.css')}}">
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/jquery.mCustomScrollbar.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin_assets/css/style.css')}}">
    <style>
    table {
        table-layout: fixed;
        width: 100%;
    }
    </style>
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <div class="mobile-search waves-effect waves-light">
                            <div class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-prepend search-close"><i
                                                class="ti-close input-group-text"></i></span>
                                        <input type="text" class="form-control" placeholder="Enter Keyword">
                                        <span class="input-group-append search-btn"><i
                                                class="ti-search input-group-text"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="ti-more"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="user-profile header-notification">
                                <a href="#!" class="waves-effect waves-light">
                                    <!-- <img src="assets/images/avatar-4.jpg" class="img-radius" alt="User-Profile-Image"> -->
                                    <span>Welcome, Admin</span>
                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li class="waves-effect waves-light">
                                        <a href="logout">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    @include('Admin/sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Manage Member</h5>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="dashboard"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item">Member Zone</li>
                                            <li class="breadcrumb-item"><a href="#">Manage Member</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-body start -->
                                    <div class="page-body">
                                        <div class="row justify-content-center">
                                            @if ($message = Session::get('message'))
                                            <div class="col-md-6 alert alert-primary alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>    
                                                <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                            @if ($message = Session::get('error'))
                                            <div class="col-md-6 alert alert-danger alert-block">
                                                <button type="button" class="close" data-dismiss="alert">×</button>    
                                                <strong>{{ $message }}</strong>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="row">
                                            <!--  sale analytics start -->
                                            <div class="col-xl-12 col-md-12">
                                                <div class="card table-card">
                                                    <div class="card-header">
                                                        <!-- <h5>Dashboard</h5> -->
                                                        <div class="card-header-right" style="padding:0px;">
                                                            <ul class="list-unstyled card-option">
                                                                <li><i class="fa fa fa-wrench open-card-option"></i>
                                                                </li>
                                                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                                                <li><i class="fa fa-minus minimize-card"></i></li>
                                                                <li><i class="fa fa-refresh reload-card"></i></li>
                                                                <li><i class="fa fa-trash close-card"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                    
                                                        <div class="form-group row"
                                                            style="display:flex;justify-content:end;">
                                                            <button class="btn bin-sm waves-effect waves-light btn-success"
                                                            style="border-radius:5px;margin:5px;"
                                                            data-toggle="modal" data-target="#ad-product">ADD</button>
                                                        </div>
                                                        <div class="table-responsive-xl" style="padding:0 10px;">
                                                            <table class="table table-responsive table-bordered"
                                                                rules="all" id="ContentPlaceHolder1_grd"
                                                                style="width:100%;border-collapse:collapse;"
                                                                cellspacing="0" cellpadding="4" border="1">
                                                                <tbody>
                                                                    <tr style="color:White;background-color:#000000;font-weight:bold;"
                                                                        align="center">
                                                                        <th scope="col">Sr. No</th>
                                                                        <th scope="col">Level</th>
                                                                        <th scope="col"
                                                                            style="color:White;background-color:#000000;font-family:verdana;font-size:12px;"
                                                                            align="left">Min Member</th>
                                                                        <th scope="col"
                                                                            style="color:White;background-color:#000000;font-family:verdana;font-size:12px;"
                                                                            align="left">Amount</th>
                                                                        <th scope="col"
                                                                            style="color:White;background-color:#000000;font-family:verdana;font-size:12px;"
                                                                            align="left">Total</th>
                                                                        <th scope="col"
                                                                            style="color:White;background-color:#000000;font-family:verdana;font-size:12px;"
                                                                            align="left">Donation</th>
                                                                        <th scope="col"
                                                                            style="color:White;background-color:#000000;font-family:verdana;font-size:12px;"
                                                                            align="left">Net Amount</th>
                                                                        <th scope="col"
                                                                            style="color:White;background-color:#000000;font-family:verdana;font-size:12px;"
                                                                            align="left">Edit</th>
                                                                    </tr>
                                                                    <?php $a = 1; ?>
                                                                    @foreach($data as $value)
                                                                    <tr style="color:#333333;background-color:#F7F6F3;border-color:#8B91A0;"
                                                                        align="center">
                                                                        <td>
                                                                            {{$a++}}
                                                                        </td>
                                                                        <td style="font-size:12px;height:30px;"
                                                                        align="left">
                                                                        <span id="ContentPlaceHolder1_grd_Label2_0">{{$value->level}}</span>
                                                                        </td>
                                                                        <td style="font-size:12px;height:30px;"
                                                                            align="left">
                                                                            <span id="ContentPlaceHolder1_grd_Label2_0">{{$value->min_member}}</span>
                                                                        </td>
                                                                        <td style="font-size:12px;height:30px;"
                                                                            align="left">
                                                                            <span id="ContentPlaceHolder1_grd_Label1_0">{{$value->amount}}</span>
                                                                        </td>
                                                                        <td style="font-size:12px;height:30px;"
                                                                            align="left"><span id="ContentPlaceHolder1_grd_Label1_0">{{$value->total}}</span></td>
                                                                        <td style="font-size:12px;height:30px;"
                                                                            align="left"><span id="ContentPlaceHolder1_grd_Label1_0">{{$value->donation}}</span></td>
                                                                        <td style="font-size:12px;height:30px;"
                                                                            align="left"><span id="ContentPlaceHolder1_grd_Label1_0">{{$value->net_amount}}</span></td>
                                                                        <td style="font-size:12px;height:30px;"
                                                                            align="left">
                                                                            <button data-toggle="modal" data-target="#edit-level" class="btn btn-sm editbtn btn-success" id="{{$value->id}}">Edit</button>
                                                                            <a onclick="return confirm('Are you sure?')" href="{{url('Admin/delete_level_setup')}}/{{$value->id}}" class="btn btn-sm btn-danger">Delete</a>
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                                    <tr class="gridviewPager">
                                                                        <td colspan="17">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  sale analytics end -->
                                        </div>
                                    </div>
                                    <!-- Page-body end -->
                                </div>
                                <div id="styleSelector"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="edit-level" tabindex="-1"
        role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <form action="{{url('Admin/update')}}" method="post">
            @csrf
            <div class="modal-dialog modal-dialog-centered"
                role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLongTitle">Edit Level Setup</h5>
                        <button type="button" class="close"
                            data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">
                                <h6>Level:*</h6>
                            </label>
                            <div class="col-sm-7">
                                <input type="hidden" name="pid" id="pid">
                                <input name="level" id="level" type="text" class="form-control"
                                    required placeholder="Level"
                                    style="border-radius:3px;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">
                                <h6>Minimum Member:*</h6>
                            </label>
                            <div class="col-sm-7">
                                <input name="min_member" id="min_member" type="text"
                                    class="form-control" required
                                    placeholder="Minimum Member"
                                    style="border-radius:3px;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">
                                <h6>Amount*</h6>
                            </label>
                            <div class="col-sm-7">
                                <input id="amount" name="amount" type="text"
                                    class="form-control" required
                                    placeholder="Amount"
                                    style="border-radius:3px;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">
                                <h6>Total*</h6>
                            </label>
                            <div class="col-sm-7">
                                <input id="total" name="total" type="text"
                                    class="form-control" required
                                    placeholder="Total"
                                    style="border-radius:3px;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">
                                <h6>Donation:*</h6>
                            </label>
                            <div class="col-sm-7">
                                <input id="donation" name="donation" type="text"
                                    class="form-control" required
                                    placeholder="Donation"
                                    style="border-radius:3px;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">
                                <h6>Net Amount:*</h6>
                            </label>
                            <div class="col-sm-7">
                                <input id="net_amount" name="net_amount" type="text"
                                    class="form-control" required
                                    placeholder="Net Amount"
                                    style="border-radius:3px;">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-secondary"
                            data-dismiss="modal">Close</button>
                        <button type="submit"
                            class="btn btn-primary">Save
                            changes</button>
                    </div>
                </div>
            </div>
        </form>
        </div>
        <!-- End Modal -->
         <!-- Modal -->
         <div class="modal fade" id="ad-product" tabindex="-1"
         role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
         <form action="{{url('Admin/add_level_setup')}}" method="post">
             @csrf
             <div class="modal-dialog modal-dialog-centered"
                 role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title"
                             id="exampleModalLongTitle">Add Level Setup</h5>
                         <button type="button" class="close"
                             data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
                     <div class="modal-body">
                         <div class="form-group row">
                             <label class="col-sm-5 col-form-label">
                                 <h6>Level:*</h6>
                             </label>
                             <div class="col-sm-7">
                                 <input name="level" type="text" class="form-control"
                                     required placeholder="Level"
                                     style="border-radius:3px;">
                             </div>
                         </div>
                         <div class="form-group row">
                             <label class="col-sm-5 col-form-label">
                                 <h6>Minimum Member:*</h6>
                             </label>
                             <div class="col-sm-7">
                                 <input name="min_member" type="text"
                                     class="form-control" required
                                     placeholder="Minimum Member"
                                     style="border-radius:3px;">
                             </div>
                         </div>
                         <div class="form-group row">
                             <label class="col-sm-5 col-form-label">
                                 <h6>Amount*</h6>
                             </label>
                             <div class="col-sm-7">
                                 <input id="amount" name="amount" type="text"
                                     class="form-control" required
                                     placeholder="Amount"
                                     style="border-radius:3px;">
                             </div>
                         </div>
                         <div class="form-group row">
                             <label class="col-sm-5 col-form-label">
                                 <h6>Total*</h6>
                             </label>
                             <div class="col-sm-7">
                                 <input id="total" name="total" type="text"
                                     class="form-control" required
                                     placeholder="Total"
                                     style="border-radius:3px;">
                             </div>
                         </div>
                         <div class="form-group row">
                             <label class="col-sm-5 col-form-label">
                                 <h6>Donation:*</h6>
                             </label>
                             <div class="col-sm-7">
                                 <input id="donation" name="donation" type="text"
                                     class="form-control" required
                                     placeholder="Donation"
                                     style="border-radius:3px;">
                             </div>
                         </div>
                         <div class="form-group row">
                             <label class="col-sm-5 col-form-label">
                                 <h6>Net Amount:*</h6>
                             </label>
                             <div class="col-sm-7">
                                 <input id="net_amount" name="net_amount" type="text"
                                     class="form-control" required
                                     placeholder="Net Amount"
                                     style="border-radius:3px;">
                             </div>
                         </div>
                     </div>
                     <div class="modal-footer">
                         <button type="submit" class="btn btn-secondary"
                             data-dismiss="modal">Close</button>
                         <button type="submit"
                             class="btn btn-primary">Save
                             changes</button>
                     </div>
                 </div>
             </div>
         </form>
         </div>
         <!-- End Modal -->
    </div>
    <!-- Required Jquery -->
    <script type="text/javascript" src="{{asset('admin_assets/js/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin_assets/js/jquery-ui/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin_assets/js/popper.js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin_assets/js/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- waves js -->
    <script src="{{asset('admin_assets/pages/waves/js/waves.min.js')}}"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{asset('admin_assets/js/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <!-- slimscroll js -->
    <script src="{{asset('admin_assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <!-- menu js -->
    <script src="{{asset('admin_assets/js/pcoded.min.js')}}"></script>
    <script src="{{asset('admin_assets/js/vertical/vertical-layout.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin_assets/js/script.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
    <script>
    let button = document.querySelector("#export");
    button.addEventListener("click", e => {
        let table1 = document.querySelector("#ContentPlaceHolder1_grd");
        TableToExcel.convert(table1);
    });
    </script>
    <script>
    function printData() {
        var divToPrint = document.getElementById("TABLE");
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }

    $('#print').on('click', function() {
        printData();
    })
    </script>
    <script>
      $(document).ready(function() {
         $('.editbtn').click(function(e) {
            e.preventDefault();
            var id = $(this).attr('id');
            // alert(id);
            $.ajax({
               url: "getSetup",
               data : {id: id},
               dataType: 'json',
               method: 'GET',
               success: function(value){
                     $('#pid').val(value.id);
                     $('#level').val(value.level);
                     $('#amount').val(value.amount);
                     $('#min_member').val(value.min_member);
                     $('#total').val(value.total);
                     $('#donation').val(value.donation);
                     $('#net_amount').val(value.net_amount);
               }
            });
         })
      });
    </script>
</body>

</html>