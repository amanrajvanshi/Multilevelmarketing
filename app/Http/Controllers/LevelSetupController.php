<?php

namespace App\Http\Controllers;

use App\Models\level_setup;
use Illuminate\Http\Request;
use Session;
class LevelSetupController extends Controller
{
    // SHOW LEVEL SETUP
    public function index()
    {
        return view('Admin/level')->with('data',level_setup::get());
    }

    //LEVEL SETUP ADD
    public function store(Request $request)
    {
        // return $request;
        $data = [
            'level' => $request-> post('level'),
            'min_member' => $request-> post('min_member'),
            'amount' => $request-> post('amount'),
            'total' => $request-> post('total'),
            'donation' => $request-> post('donation'),
            'net_amount' => $request-> post('net_amount'),
        ];
        $res = level_setup::insert($data);
        if($res){
            Session::flash('message','Level Setup Added!');
            return redirect('Admin/level_setup');
        }else{
           Session::flash('error','Level Setup Not Added!');
            return redirect('Admin/level_setup');
        }
    }

    public function getSetup(Request $request)
    {
        $id = $request->id;
        $data = level_setup::find($id);
        return response()->json(['id' => $data->id, 'level' => $data->level,'amount' => $data->amount,'total'=>$data->total,'min_member' => $data->min_member,'donation'=>$data->donation,'net_amount'=>$data->net_amount]);
    }

    public function update(Request $request)
    {
        // return $request;
        $id = $request-> post('pid');
        $data = [
            'level' => $request-> post('level'),
            'min_member' => $request-> post('min_member'),
            'amount' => $request-> post('amount'),
            'total' => $request-> post('total'),
            'donation' => $request-> post('donation'),
            'net_amount' => $request-> post('net_amount'),
        ];
        $res = level_setup::where('id','=',$id)->update($data);
        if($res){
            Session::flash('message','Level Setup Updated!');
            return redirect('Admin/level_setup');
        }else{
           Session::flash('error','Level Setup Not Updated!');
            return redirect('Admin/level_setup');
        }
    }

    public function destroy($id)
    {
        $res = level_setup::destroy($id);
        if($res){
            Session::flash('message','Level Setup Deleted!');
            return redirect('Admin/level_setup');
        }else{
           Session::flash('error','Level Setup Not Deleted!');
            return redirect('Admin/level_setup');
        }
    }
}
