<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\user_parent;
use App\Models\Income;
use App\Models\level;
use App\Models\level_setup;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Session;
class WebController extends Controller
{
    public function getUser(Request $request)
    {
        $data = User::where('member_id','=',$request->data)->get();
        // $data = User::where('member_id','=',$request->data)->where('status','=','Active')->get();
        foreach($data as $value){}
        return response()->json(['id'=>$value->id,'name'=>$value->name]);
    }
    public function index(){
        return view('web.register');
    }
    //User registration
    public function post(Request $request){
        $id  = $request->pid;
         //For fetching levelwise child
        //  $level = user_parent::orderBy('id','DESC')->where('member_id', $request->pid)->get('parent_id');
        //  $data = [];
        //  foreach ($level as $l){
        //      $data[] = $l->parent_id;
        //  }
        //  return $data;
        $request->validate([
            'sponsor_id' => 'required',
            'sponsor_name' => 'required',
            'name' => 'required',
            'mobile_no' => 'required',
            'email' => 'required',
            'password' => 'required',
            'confirm_password' => 'required',
        ]);

        $mid = User::max('id');
        // return $mid+1;
        if($mid < 9){
        $member_id = 'GF1' . "0000" . $mid+1;
        }
        elseif($mid < 99){
            $member_id = 'GF1' . "000" . $mid+1;
        } 
        elseif($mid < 999){
            $member_id = 'GF1' . "00" . $mid+1;
        }else{
            $member_id = 'GF1' . "0" . $mid+1;
        }
        $User = new User();
        $User->member_id = $member_id;
        $User->sponsor_id = $request->pid;
        $User->name = $request->name;
        $User->mobile_no = $request->mobile_no;
        $User->status = 'Inactive';
        $User->top_up = "no";
        $User->product = 1;
        $User -> joining_date_from=date('Y-m-d H:i:s');
        $User->email = $request->email;
        $User->password = md5($request->password);
        $count = User::where('sponsor_id','=',$request->pid)->count();
        // return $count;
        $User->save();
        if($User->save()){
            $max = User::max('id');
            if($request->pid){
                $data = user_parent::where('member_id','=',$request->pid)->get();
            }
            if(isset($data)){
                if(count($data) > 0){
                    $rr = DB::insert("INSERT INTO user_parents (member_id, parent_id,level_parent) VALUES($max,$request->pid,1)");
                    if($rr){
                      $res=  DB::insert("INSERT INTO user_parents (member_id, parent_id,level_parent) SELECT $max, parent_id,level_parent+1 FROM user_parents WHERE member_id=$request->pid AND parent_id != 0");
                    }
                }

                if($res)
                {
                    //Fetch the data from level_setup table
                    $level_data = level_setup::get();
                    //For fetching levelwise child
                    $p_data = user_parent::where('member_id','=',$request->pid)->get('parent_id');
                    $dd = [];
                    foreach($p_data as $vv){
                        $dd[] = $vv -> parent_id;
                    }
                    // print_r($dd);
                    // exit;
                    $data = array();
                    $x = 1;
                    foreach ( $level_data as $l){
                        $level_id=$l->id;
                        // return $level_id;
                        $minm = $l->min_member;
                        return $minm;
                        // $le_data = user_parent::whereIn('level_parent',$dd)->get();
                        // return $le_data;
                        // return $request->pid;
                        $sql="select parent_id,(select count(id)from user_parents where parent_id=ua.parent_id) as count_child from user_parents as ua where member_id=$max and level_parent=$level_id";

                        $ress = DB::SELECT($sql);
                       
                        if(count($ress)>0)
                        {
                            foreach ($ress as $vad){}
                            // return $vad -> parent_id;
                        // exit;
                        if($vad -> parent_id != ''){
                            if($vad->count_child == $minm){
                                $dat = [
                                    'member_id'  => $vad -> parent_id,
                                    'income_by'  => $max,
                                    'amount'   => $l -> amount,
                                    'income_type' => $l->id,
                                    'description' => "Level $l->id Income",
                                    'transaction_type' => 'Credit',
                                    'net_amount' => $l -> amount,
                                ];
                                $ress = Income::insert($dat);
                                if($ress){
                                    $cid = Income::where('member_id','=',$request->pid)->get();
                                    foreach($cid as $ccid){
                                        // write all donation here
                                        $res = new Income;
                                        $res -> member_id  = $ccid -> member_id;
                                        $res -> income_by  = '';
                                        $res -> amount   = ($l -> amount * $l -> percentage)/100;
                                        $res -> income_type = '';
                                        $res -> description = "Donation";
                                        $res -> transaction_type = 'Debit';
                                        $res -> net_amount =($l -> amount * $l -> percentage)/100;
                                        $res -> save();
                                    }
                                    $rank_update = [
                                        'level' => $l -> rank,
                                    ];
                                    $update_rank = User::where('id','=',$ccid -> member_id)->update($rank_update);
                                    if($update_rank){
                                        $res_update = new Income;
                                        $res_update -> member_id  = $ccid -> member_id;
                                        $res_update -> income_by  = '';
                                        $res_update -> amount   = $l->rank_up_charge;
                                        $res_update -> income_type = '';
                                        $res_update -> description = "Rank Updation Charge";
                                        $res_update -> transaction_type = 'Debit';
                                        $res_update -> net_amount =$l->rank_up_charge;
                                        $res_update -> save();
                                    }
                                }
                            }else if($vad->count_child > $minm){
                                $data[] = array(
                                    'member_id'  => $vad -> parent_id,
                                    'income_by'  => $max,
                                    'amount'   => $l -> amount,
                                    'income_type' => $l->id,
                                    'description' => "Level $l->id Income",
                                    'transaction_type' => 'Credit',
                                    'net_amount' => $l -> amount,
                                );  
                            }else{
                                $data[] = array(
                                    'member_id'  => $vad -> parent_id,
                                    'income_by'  => $max,
                                    'amount'   => $l -> amount,
                                    'income_type' => $l->id,
                                    'description' => "Level $l->id Income",
                                    'transaction_type' => 'Credit',
                                    'net_amount' => $l -> amount,
                                );
                               
                            }
                        }
                        }
                        
                        $x++;
                    }
                 
                    income::insert($data);
                }


               
               
                // foreach($data as $va){
                //     // echo $va;
                //     $child = user_parent::where('parent_id','=',$va)->where('level_parent','=',$va)->get();
                //     $ls = level_setup::where('id','=',$va)->get();
                //     // return $ls;
                //     foreach($ls as $la){}
                //     if(count($child) == $la->min_member){
                       
                //         income::insert($data);
                //     }else if(count($child) > $la->min_member){
                //         $data = [
                //             'member_id'  => $va,
                //             'income_by'  => $max,
                //             'donation'   => $la -> donation,
                //             'income_type' => 'Direct',
                //             'description' => "Level $la->id Donation",
                //             'transaction_type' => 'Credit',
                //             'net_amount' => $la -> net_amount,
                //         ];
                //         income::insert($data);
                //     }
                    
                // }
                // exit;
                
            }else{
                DB::insert("INSERT INTO user_parents (member_id, parent_id) VALUES ($max,0)");
            }
            return redirect('User');
        }
       
    }
    public function edit($id)
    {
        // return $id;
        // return User::find($id);
        return view('Admin/edit_user')->with('data',User::find($id));
    }
    public function edit_by_user($id)
    {
        return view('User/edit_user')->with('data',User::find($id));
    }
    public function update(Request $req)
    {
        // return $req;
        $id = $req -> post('pid');
        $sponser_id = $req -> post('sponser_id');
        $member_id = $req -> post('member_id');
        $data = User::find($id);
        // return [$member_id,$sponser_id,$id];
        // return $data -> member_id;
        if($data -> top_up == 'Paid' && $data -> member_id == $member_id){
             $result =  DB::table('users') 
             ->where('id', $id)
             ->limit(1) 
             ->update(['status'=>'Active','activation_date_from'=>date('d-m-Y H:i:s')]);
             if($result){
                 Session::flash('message','User Activated successfully!');
                 return redirect('Admin/new-registration');
             }else{
                Session::flash('error','User Not Activated!');
                 return redirect('Admin/new-registration');
             }
        }else{
            return "No";
        }
       
    }
    public function update_by_user(Request $req)
    {
        $id = $req -> post('pid');
        $sponser_id = $req -> post('sponsor_id');
        $member_id = $req -> post('member_id');
        $data = User::find($id);
        // return [$member_id,$sponser_id,$id];
        // return $data -> member_id;
        if($data -> top_up == 'Paid' && $data -> member_id == $member_id){
             $result =  DB::table('users') 
             ->where('id', $id)
             ->limit(1) 
             ->update(['status'=>'Active','activation_date_from'=>date('d-m-Y H:i:s')]);
             if($result){
                 Session::flash('message','User Activated successfully!');
                 return redirect('User/new-registration');
             }else{
                Session::flash('error','User Not Activated!');
                 return redirect('User/new-registration');
             }
        }else{
            return "No";
        }
       
    }
}
