<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_setups', function (Blueprint $table) {
            $table->id();
            $table->string('level')->nullable();
            $table->string('min_member')->nullable();
            $table->string('amount')->nullable();
            $table->string('total')->nullable();
            $table->string('donation')->nullable();
            $table->string('net_amount')->nullable();
            $table->string('percentage')->nullable();
            $table->string('rank')->nullable();
            $table->string('rank_up_charge')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_setups');
    }
}
